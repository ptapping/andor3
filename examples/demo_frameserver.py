#/usr/bin/env python3

import sys
import logging

# Use PySide6 Qt6 bindings for user interface. PyQt5, PySide2, PyQt6 should work fine too
from PySide6 import QtWidgets
from PySide6.QtCore import Signal
import numpy as np
import pyqtgraph as pg
import andor3


class FrameServerDemo(QtWidgets.QWidget):
    """
    Demonstrate the use of the FrameServer helper to stream frames from the camera to a Qt GUI
    application.
    
    The image data is presented as a ImageItem inside a pyqtgraph plot. The pyqtgraph plotting
    library is chosen for this application because of its high-performance realtime plotting.
    """

    # A Qt Signal is needed to bridge the FrameServer native python thread
    # to the Qt event loop thread for safely updating the user interface
    image_acquired = Signal(np.ndarray)
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Andor3 FrameServer Demo")

        # Create the pyqtgraph GraphicsLayoutWidget and add it to ourself
        self.glw = pg.GraphicsLayoutWidget()
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.glw)

        # Create the ploting axes in the GraphicsLayoutWidget
        self.plot = self.glw.addPlot()
        self.plot.setLabels(left="Pixel #", bottom="Pixel #")
        # Create the image item and add it to the plot
        self.image = pg.ImageItem()
        self.plot.addItem(self.image)

        try:
            # Initialise the camera and start acquiring frames using the FrameServer helper
            self.log.info("Initialising Andor3 camera...")
            self.cam = andor3.Andor3()
            self.log.info(f"Found {self.cam.CameraFamily} {self.cam.CameraModel} {self.cam.CameraName} {self.cam.InterfaceType}")
            # Do whatever camera configuration desired here...
            self.cam.SensorCooling = True
            self.cam.FanSpeed = "On"
            self.cam.TriggerMode = "Internal"
            self.cam.ExposureTime = 0.001
            self.cam.FrameRate = 30
            #self.cam.FrameRate = self.cam.max("FrameRate")//2
            self.log.info(f"Acquiring at {self.cam.FrameRate} fps.")
            self.cam.ElectronicShutteringMode = "Rolling"
            self.cam.SimplePreAmpGainControl = "16-bit (low noise & high well capacity)"
            self.cam.PixelEncoding = "Mono16"
            self.cam.SpuriousNoiseFilter = False
            self.cam.StaticBlemishCorrection = False
            self.cam.MetadataEnable = False
            self.cam.FastAOIFrameRateEnable = True
            self.cam.AOIHeight = self.cam.max("AOIHeight")
            self.cam.VerticallyCentreAOI = True
            self.cam.AOILeft = 1
            self.cam.AOIWidth = self.cam.max("AOIWidth")

            # Create the FrameServer helper and start it serving frames in a background thread
            # It will call the frame_callback method when new image data is available
            self.log.info("Starting FrameServer...")
            self.fsvr = andor3.FrameServer(self.cam, self.frame_callback, completion_callback=self.acquisition_completed)
            self.fsvr.start(frame_rate_max=60)
        except:
            self.log.exception("Unable to initialise Andor3 camera!")

        # Connect the image acquired Signal to a handler
        self.image_acquired.connect(self.update_image)


    def frame_callback(self, n, data, timestamp):
        """
        Handle image data streamed by the Andor3 FrameServer.

        This just emits the Qt Signal so that the UI can then be updated within the Qt event loop.
        Any changes to the Qt UI elements should only be performed within the Qt event loop thread,
        otherwise bad things will happen...
        """
        self.image_acquired.emit(data)


    def update_image(self, data):
        """
        Update the plot with new image data.
        
        This should only be called from within the Qt event loop thread, such as when the
        appropriate Signal is emitted.
        """
        self.image.setImage(data)


    def acquisition_completed(self, frame_count):
        self.log.info(f"Acquisition completed, {frame_count} frames received.")
    

    def closeEvent(self, event):
        """Handler for window close event."""
        try:
            self.log.info("Stopping FrameServer...")
            self.fsvr.stop()
            self.cam.close()
            self.log.info("Camera closed.")
        except:
            self.log.exception("Error attempting to stop FrameServer.")


if __name__ == "__main__":
    # Initialise the Qt app and run the event loop
    app = QtWidgets.QApplication(sys.argv)
    window = FrameServerDemo()
    window.show()
    sys.exit(app.exec_()) # PySide6 really wants app.exec() here now
