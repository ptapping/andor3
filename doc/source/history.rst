Version History
===============

.. note::
    Until version 1.0.0 is released, consider that this package is under development. This means
    that the API may change without notice, however the interface to :class:`~andor3.andor3.Andor3`
    can be considered fairly stable at this time.


0.3.4 (2022-08-10)
------------------
  - Add "Program Files\Andor SDK3" to DLL search locations on Windows.


0.3.3 (2021-12-15)
------------------

  - Fix bug when attempting to stop FrameServer/FrameDump from inside callback.


0.3.2 (2021-09-23)
------------------

  - FrameServer option to restrict frame rate.
  - Fix spelling of FastAOIFrameRateEnable feature.


0.3.1 (2021-09-15)
------------------

  - FrameDump returns the number of frames actually acquired, rather than padding with blank frames.


0.3.0 (2021-08-04)
------------------

  - Utilities for simplified image streaming and acquisition.
  - Image decoding including metadata fields.


0.2.0 (2021-05-11)
------------------

  - All features of the Andor SDK3 camera API implemented.
  - Basic image decoding.