Getting Started
===============

Prerequisites
-------------

The only true dependency is the `numpy <https://numpy.org/>`_ library, however it is highly
recommended to have the `numba <https://numba.pydata.org/>`_ just-in-time (JIT) compiler available.

Installing the Software
-----------------------

Download Using Pip
^^^^^^^^^^^^^^^^^^

The package installer for Python (pip) is the typical method for installation:

.. code-block:: sh

    pip install --user --upgrade andor3

The ``--user`` parameter installs using user-level permissions, thus does not require root or
administrator privileges. To install system wide, remove the ``--user`` parameter and prefix the
command with ``sudo`` (Linux, MacOS), or run as administrator (Windows).

The `numba <https://numba.pydata.org/>`_ library is not a hard dependency, and so is not
automatically installed. It is recommended to install it, for example:

.. code-block:: sh

    pip install --user --upgrade numba

If the install fails, it may be because your system architecture or python version does not have a
pre-compiled numba package in "wheel" format. Compiling from the "sdist" source package, or from the
`source code <https://numba.readthedocs.io/en/stable/user/installing.html#installing-from-source>`__
should work, but requires a system install of llvm. See the `install instructions for llvmlite
<http://llvmlite.pydata.org/en/latest/admin-guide/install.html>`__ for more information.


Clone From Git
^^^^^^^^^^^^^^

Alternatively, the latest version can be downloaded from the git repository:

.. code-block:: sh

    git clone https://gitlab.com/ptapping/andor3.git

and optionally installed to your system using ``pip``:

.. code-block:: sh

    pip install --user ./andor3


Install Andor Software
^^^^^^^^^^^^^^^^^^^^^^

The proprietary Andor libraries are required to be present on the system (either Linux or Windows).
Unfortunately, the licensing means they can not be distributed with this package, or freely
downloaded. The libraries will be attempted to be found in several common locations. Install Andor
software such as the Andor Driver Pack, Andor Solis, or the Andor LabView SDK, and the libraries
should be detected automatically.


Usage
-----

.. code-block:: python

    from andor3 import Andor3
    cam = Andor3()
    
    # Configure capture settings using the various camera features
    # This example is for a Zyla, features may not apply to other models
    cam.SensorCooling = True
    cam.FanSpeed = "On"
    cam.CycleMode = "Fixed"
    cam.AccumulateCount = 1
    cam.TriggerMode = "Internal"
    cam.ExposureTime = 0.01
    cam.ElectronicShutteringMode = "Rolling"
    cam.Overlap = True
    cam.SimplePreAmpGainControl = "16-bit (low noise & high well capacity)"
    cam.PixelReadoutRate = "280 MHz"
    cam.PixelEncoding = "Mono16"
    cam.SpuriousNoiseFilter = False
    cam.StaticBlemishCorrection = False
    cam.MetadataEnable = False
    
    # Capture only the middle 128 pixels of the sensor
    cam.AOIHeight = 128
    cam.AOILeft = 1
    cam.AOIWidth = 2560
    cam.VerticallyCentreAOI = True

    # Create a buffer for the image data to be stored in
    cam.queueBuffer()
    # Start the acquisition and retrieve the raw data from the buffer
    cam.start()
    data = cam.waitBuffer(timeout=1000)
    cam.stop()
    # Decode the raw data to an image (as 2D numpy array)
    img = cam.decode_image(data)
    

To see all valid features for a connected camera:

.. code-block:: python

    print(cam.features)

.. code-block:: none

    ['AccumulateCount', 'AcquisitionStart', 'AcquisitionStop',
     'AOIBinning', 'AOIHeight','AOILeft', 'AOIStride', ...]


Any valid feature name can be accessed as a property of an :class:`~andor3.andor3.Andor3` object.
For example:

.. code-block:: python

    print(f"{cam.CameraFamily} {cam.CameraModel} {cam.CameraName} {cam.InterfaceType}")

.. code-block:: none

    Andor sCMOS DG152XC0EFI219 Zyla CL 10 Tap

or

.. code-block:: python

    cam.ExposureTime = 0.1
    cam.TriggerMode = "External"
    cam.SpuriousNoiseFilter = False

To see details about avaliable camera features, such as data type and valid ranges:

.. code-block:: python

    print(cam.describe_features())

.. code-block:: none

    AccumulateCount: Integer (rw)
    value=1 min=1 max=2.14748e+09
    AcquisitionStart: Command (rw)
    AcquisitionStop: Command (rw)
    AOIBinning: Enumerated (rw)
    ->  0 : 1x1
        1 : 2x2
        2 : 3x3
        3 : 4x4
        4 : 8x8
    AOIHeight: Integer (rw)
    value=2160 min=8 max=2160
    AOILeft: Integer (rw)
    value=1 min=1 max=1
    AOIStride: Integer (r-)
    value=5120 min=40 max=10240
    AOITop: Integer (rw)
    value=1 min=1 max=1
    AOIWidth: Integer (rw)
    value=2560 min=40 max=2560
    AuxiliaryOutSource: Enumerated (rw)
        0 : FireRow1
        1 : FireRowN
    ->  2 : FireAll
        3 : FireAny
    ...

Note that the above demonstrates a very "raw" method of interfacing to the Andor SDK. There are
convenience classes and function in the :mod:`andor3.utils` package which make the streaming of
frame data (:class:`~andor3.utils.FrameServer`) or the bulk acquisition of frames
(:class:`~andor3.utils.FrameDump`) greatly simplified.