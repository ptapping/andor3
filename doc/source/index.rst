Welcome to andor3's documentation!
===============================================

This is an interface to Andor camera devices which communicate using version 3 of the Andor Software
Development Kit (Andor SDK3).

It depends on the Andor SKD3 being installed on the system (``libatcore.so.3`` etc on Linux,
``atcore.dll`` etc on Windows). These are proprietary, non-free, and closed-source, and thus unable
to be distributed with this package. If you feel that those licensing terms restrict the
functionality of the very expensive camera you own, and hinder the progress of your project or
research, please write to Andor/Oxford Instruments to complain about that.

While this package should be usable without knowing the details of the SDK3, consultation of the
SDK3 documentation is recommended for details about camera features etc. Unfortunately, the
documentation is also not freely available.

Tested using an Andor Zyla (both USB 3.0 and CameraLink interfaces), however any camera compatible
with the SDK3 should work, such as the Neo, Apogee, or i-Star sCMOS.

The source code is available at `<https://gitlab.com/ptapping/andor3>`_.

Documentation online at `<https://andor3.readthedocs.io/en/latest/>`_.

Python Package Index (PyPI) page at `<https://pypi.org/project/andor3/>`_.


User Guide
----------

.. toctree::
   :maxdepth: 2

   gettingstarted
   history

API Documentation
-----------------
.. toctree::
   :maxdepth: 5
   :titlesonly:

   api/andor3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
